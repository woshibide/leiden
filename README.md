# Leiden

## [Creative Programming](https://studiegids.universiteitleiden.nl/en/courses/117924/creative-programming)

Rob Saunders
r.saunders@liacs.leidenuniv.nl

Edwin van der Heide
e.f.van.der.heide@liacs.leidenuniv.nl

## [Video Games for Research](https://studiegids.universiteitleiden.nl/en/courses/117940/video-games-for-research)

Barbero Giulio
g.barbero@liacs.leidenuniv.nl


