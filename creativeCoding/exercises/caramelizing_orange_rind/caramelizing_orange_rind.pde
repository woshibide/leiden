int SEED = 32;
int MULT = 2;
int MOD = 2;

void setup() {
  background(25,25,25);
  
  
  size(800,800);
  scale(0.6);
  
  fill(75, 50, 220); // purple
  pattern(200, 200, 25, 30);
  
  fill(175, 150, 50); // yellow
  pattern(80, 400);
}

void pattern(float x, float y) {
  for (int n = 1; n <= SEED; n++) {
    float dx = n * 10;
    ellipse(x + dx,      y,
            x + dx * MULT, y * MOD);
} }

void pattern(float x, float y,
             float w, float h) {
  for (int n = 1; n <= SEED * MULT; n++) {
    float dx = n * w;
    ellipse(x + dx,     y,
            x + dx + w, y + h);
} }


//void draw(){
  
//  pushMatrix();
  
//  for( int k = 1; k <= 8; k++){
//    pattern(k, k);
//    pushMatrix();
//    rotate(-PI/4);
//    popMatrix();
//    for (int j = 1; j <= 64; j++) {
//      pushMatrix();  
//      pattern(j, k + j);
//      popMatrix();
//      scale(0.2);
//      j += 3;
//    }
//  }
//}
