float in = 2; // curvature of the galactic arm
int maxIt = 2000; // max iteration number
float scale = 4; // radial multiplicative factor
float cut = 100; // radial cutoff
float f = 50; // final scale factor
float expFactor = 0.9; // scaling factor

float theta, theta2;
float r;
float x, y;

void setup() {
  background(0);
  size(600, 600);
  strokeWeight(1);
  stroke(100, 50);
}

void draw(){
  galaxy();
}

void galaxy() {
  translate(width / 2, height / 2); // Move the origin to the center of the window

  for (int i = 0; i <= maxIt; i ++) { // first arm of the galaxy
    theta = i / 50.0;
    r = scale * expFactor * (theta * tan(in));
    
    
    
    if (r > cut) {
      break;
    }

    x = r * cos(theta);
    y = r * sin(theta);

    point(x + f * random(0, 1), y + f * random(0, 1));
  }
  for (int j = 0; j <= maxIt; j ++) { // second arm of the galaxy fails to be drawn
  theta = j / 50.0;
  theta2 = (float(j) / 50.0) - PI;
  
  r = scale * expFactor * (theta2 * tan(in));
  
  println("Iteration " + j + ", r = " + r + ", j = " + j); // new debug statement

  if (r > cut) {
    break;
  }

  x = r * cos(theta);
  y = r * sin(theta);

  point(x + f * random(0, 1), y + f * random(0, 1));
}

}
