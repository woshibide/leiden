float x1, y1, x2, y2;
int alpha = 255;
float lineLength = 10;

void setup() {
  size(800, 600);
  background(0);
  x1 = width / 2;
  y1 = height / 2;
  x2 = x1 + lineLength;
  y2 = y1;
}

void draw() {
  stroke(255, 20);
  lineLength = random(10, 50);
  line(x1, y1, x2, y2);
  stroke(255);
  x1 = x2;
  y1 = y2;
  
  // Random 90-degree turn
  float r = random(1);
  if (r < 0.25) {
    // Turn right
    x2 = x1;
    y2 = y1 + lineLength;
  } else if (r < 0.5) {
    // Turn left
    x2 = x1;
    y2 = y1 - lineLength;
  } else if (r < 0.75) {
    // Turn up
    x2 = x1 - lineLength;
    y2 = y1;
  } else {
    // Turn down
    x2 = x1 + lineLength;
    y2 = y1;
  }
  
  // Wrap around the edges
  x1 = (x1 + width) % width;
  y1 = (y1 + height) % height;
  x2 = (x2 + width) % width;
  y2 = (y2 + height) % height;
  
}
