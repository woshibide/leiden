int NUM = 1;


void setup () {
  PFont font = createFont("FT88-School.otf", 96);
  textAlign(CENTER, BASELINE);
  size (700, 1000);
  noStroke();
  strokeWeight(5);
  shapeMode(CENTER);
  background (255);
  frameRate(60);
  
  
}

void draw() {
  
  filter(BLUR);
  
  //fill(175, 250, 50);
  //rect(width / 2, height / 2, width * NUM, height * NUM);
  
  //fill(15);
  //rect(width / 2 , height / 2, width * NUM, - height * NUM);
  
  //filter (BLUR);
  //translate(width/2, height/2);
  
  
  
  float x = width / 2 * NUM - width * noise(millis() * 0.017);
  float y = height / 2 * NUM - height * noise(millis() * 0.11);
  int z = int(NUM * NUM * noise(millis() * 1));
  //fill (int(random(0, 255)) % NUM, 1);
  
  // foo(x, y, 10); // pattern
  
  

  
  // somehow feels more effective on height being processed first
  for (int xWP = 0; xWP <= height / 1000; xWP ++){  // xWP x width pattern applied 
    println("xwp cycle");
          
    rotate(cos((TWO_PI * xWP * NUM)));
    for (int yWP = 0; yWP <= width / 1000; yWP ++){
      foo(x, y, z);

      //println("ywp cycle - ", yWP);
    }
  }
}


void foo(float x, float y, int depth){
  for (int j = 0; j <= depth * NUM; j ++){
    rotate(tan((TWO_PI * j * NUM)));
    for (int i = 0; i < NUM * NUM; i++) {
      text("hello", (x + width) % width, (y + height) % height);
      scale(0.002);
      fill (50, 150, 220, 50);
      translate(width / 2, height * 4);
      rotate(cos((TWO_PI * i * NUM)));
      rect(x * i, i * NUM, x, y);
      //scale(0.5);
      
    }
  }
}
