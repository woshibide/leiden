void setup() {
  size(250, 500);
  noStroke();
  fullScreen();
  frameRate(10);
  scale(2);
  background(25);
  // noLoop();
}

void draw() {
  
  // flame colors
  int R = int(random(200,255)); 
  int G = int(random(25, 180)); 
  int B = int(random(0, 10)); 
  fill(R, G, B, 30);
  
  ogonyok(100);
}

void ogonyok(int flame_size){
  
  beginShape();
    // flame starting point
    vertex(flame_size, height - 10); // control point
 
     // flame peak
    bezierVertex(
      random(- flame_size, flame_size), random(flame_size, flame_size * 2), // first handle
      random(flame_size, flame_size * 1.5), random(flame_size, flame_size * 2), // second handle
      random(flame_size, flame_size * 2), random(0, flame_size) // control point
    );
    
     //flame ending point
     bezierVertex(
     random(flame_size * 1.5, flame_size * 2), random(flame_size, flame_size * 2), // first handle
     random(flame_size * 2, flame_size * 3), random(flame_size, flame_size * 2), // second handle
     flame_size * 2, height - 10 // control point
     );
    
  endShape();
};
