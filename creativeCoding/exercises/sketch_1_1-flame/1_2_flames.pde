void setup() {
  size(250, 500);
  fullScreen();
  frameRate(30);
  scale(2);
  noStroke();
//  noLoop();
}

void draw() {

  for (float y = height/2; y <= height/2; y += 10) {  
    
    for (float x = 1; x <= width; x += width / random(2, 24)) {
      int R = int(random(200,255)); 
      int G = int(random(25, 180)); 
      int B = int(random(0, 10)); 
      fill(R, G, B, 10);
      
      ogonyok(int(x), 0.5);
      
    }
    endShape();
  }  
}


void ogonyok(int flame_size, float scale){
  
  beginShape();
    // flame starting point
    vertex(flame_size, height / scale); // control point
 
     // flame peak
    bezierVertex(
      random(- flame_size, flame_size), random(flame_size, flame_size * 2 * scale), // first handle
      random(flame_size, flame_size * scale), random(flame_size, flame_size * 2 * scale), // second handle
      random(flame_size, flame_size * 2 * scale), random(0, flame_size) // control point
    );
    
     //flame ending point
     bezierVertex(
     random(flame_size * 1.5 * scale, flame_size * 2 * scale), random(flame_size, flame_size * 2 * scale), // first handle
     random(flame_size * 2 * scale, flame_size * 3 * scale), random(flame_size, flame_size * 2 * scale), // second handle
     flame_size * 2 * scale, height // control point
     );
    
  endShape();
};
