float[] x = new float[0];
float[] y = new float[0];
float[] r = new float[0];
float[] g = new float[0];
float[] b = new float[0];

void setup() {
  size(400, 400);
  scale(2);
  fullScreen();

  background(25);

  noSmooth();
  frameRate(1000);
}

void draw() {
  for (int i = 0; i < x.length; i++) {
    x[i] += random(i, 5);
    y[i] += noise(i, 5);

    if (x[i] < 0) {
      x[i] = width;
    }
    if (x[i] > width) {
      x[i] = 0;
    }

    if (y[i] < 0) {
      y[i] = height * 2;
    }
    if (y[i] > height) {
      y[i] = 0;
    }

    stroke(r[i], g[i], b[i]);
    point(x[i], y[i]);
  }
}

void mousePressed() {
  x = append(x, mouseX);
  y = append(y, mouseY);
  r = append(r, random(0, 50));
  g = append(g, random(200, 256));
  b = append(b, random(0,50));
}
