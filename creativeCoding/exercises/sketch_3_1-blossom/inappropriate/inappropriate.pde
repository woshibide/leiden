PFont compagnon;

import java.util.Arrays;
import java.util.Comparator;

PImage[] images;
int imageSize = 200;
int imgIndex = 0;
int alphaCh = 0;

void setup() {
  compagnon = createFont("Compagnon-LightItalic.otf", 32);
  textFont(compagnon);
  textSize(24);
  textAlign(CENTER);
  smooth(4);
  
  size(400, 400);
  frameRate(24);
  loadImages(); // Load images from the 'data' folder
}

void draw() {
  background(255);

  if (images != null && images.length > 0) {
    
    // Calculate the index based on mouse X position using map()
    imgIndex = int(constrain(map(mouseY, 
                                 height, 0, 0, images.length - 1),
                             0, images.length - 1) % images.length);
    
    alphaCh = int(map(imgIndex, 0, images.length, 0, 100));
    
    println(alphaCh);
    
    fill(0, alphaCh);
    text("lets fuck", width / 2, height - height / 8);
    
     //Display the selected image
    image(images[imgIndex], 100, 100);
  } else {
    fill(0);
    text("no images on input", 10, height - 10);
  }
}

void loadImages() {
  // Get the 'data' folder
  File folder = dataFile("");
  
  // Get a list of files in the 'data' folder
  File[] files = folder.listFiles(); 
  
  // Sort the files by name
  Arrays.sort(files, new Comparator<File>() {
    public int compare(File f1, File f2) {
      return f1.getName().compareTo(f2.getName());
    }
  });
  
  ArrayList<PImage> imageList = new ArrayList<PImage>();
  
  for (File file : files) {
    if (file.getName().toLowerCase().endsWith(".png")) {
      PImage img = loadImage(file.getPath());
      img.resize(imageSize, imageSize);
      imageList.add(img);
    }
  }
  
  // Convert the ArrayList to an array
  images = imageList.toArray(new PImage[imageList.size()]);
}
