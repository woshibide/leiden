import java.util.Arrays;
import java.util.Comparator;

PImage[] images;
int imageSize = 200;
int imgIndex = 0;
int alphaCh = 0;

int j = 25; // default num of hehehe
String[] texts; // array to hold the texts
PVector[] positions; // array to hold the positions
float[] textSizes; // array to hold the text sizes

float angle = 0, speed = 3, radius = 50;
float sx = 1, sy = 20;

void setup(){
 frameRate(24);
 size(400, 400);
 loadImages();
 generateText();
 textAlign(CENTER, CENTER);
}

void draw() {
  background(255);
  
  float x1 = images.length + cos(radians(angle)) * radius;
  float y1 = images.length + sin(radians(angle)) * radius;
  // ellipse(x1, y1, 2, 2);
  float x2 = x1 + cos(radians(angle * sx)) * radius/2;
  float y2 = y1 + sin(radians(angle * sy)) * radius/2;
  // ellipse(x2, y2, 10, 10);
  angle += speed;

  if (images != null && images.length > 0) {
    
    imgIndex = int(constrain(map(images.length, 0, y2, 0, images.length - 1),
                              0, images.length - 1) % images.length);
    alphaCh  = int(constrain(map(images.length, y2, 0, 0, images.length - 1), 
                              0, images.length - 1) % images.length);
    
  for (int i = 0; i < j; i++) {
      fill(0, alphaCh);
      textSize(textSizes[i]);
      text(texts[i], positions[i].x, positions[i].y);
    }
    
    // Display the selected image
    image(images[imgIndex], 100, 100);
  } else {
    fill(0);
    text("No input", width/2, height/2);
  }
}

void hehehe() {
  textSize(random(10, 40)); // Randomize text size
  fill(0, alphaCh); // Set text color with alpha
  textAlign(CENTER, CENTER);
  text("hehehehehehhe", width/2, height/2);
}

void generateText() {
  texts = new String[j];
  positions = new PVector[j];
  textSizes = new float[j];

  for (int i = 0; i < j; i++) {
    texts[i] = "hehehehehehhe";
    positions[i] = new PVector(random(width), random(height));
    textSizes[i] = random(10, 40);
  }
}

void loadImages() {
  File folder = dataFile(""); // Get the 'data' folder
  File[] files = folder.listFiles(); // Get a list of files in the 'data' folder
  
  // Sort the files by name
  Arrays.sort(files, new Comparator<File>() {
    public int compare(File f1, File f2) {
      return f1.getName().compareTo(f2.getName());
    }
  });
  
  ArrayList<PImage> imageList = new ArrayList<PImage>();
  
  for (File file : files) {
    if (file.getName().toLowerCase().endsWith(".png")) {
      PImage img = loadImage(file.getPath());
      img.resize(imageSize, imageSize);
      imageList.add(img);
    }
  }
  
  // Convert the ArrayList to an array
  images = imageList.toArray(new PImage[imageList.size()]);
}
