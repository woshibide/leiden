PImage img;
PImage sorted;
int selectedPixel;

void setup() {
  size(400, 200);
  img = loadImage("input.jpg");
  sorted = createImage(img.width, img.height, RGB);

  sorted.loadPixels();
  img.loadPixels();

  for (int i = 1; i < sorted.pixels.length; i++) {
    float max = -1;
    selectedPixel = i;
    println(i);
    for (int j = i; j < sorted.pixels.length; j++) {
      color pix = img.pixels[j];
      float b = brightness(pix);
      if (b > max) {
        selectedPixel = j;
        max = b;
      }
    
    color temp = sorted.pixels[i];
    sorted.pixels[j] = img.pixels[selectedPixel];
    sorted.pixels[selectedPixel] = temp;
    }
 
  }

  img.updatePixels();
  sorted.updatePixels();
}

void draw() {
  background(255);
  image(img, 0, 0);
  image(sorted, img.width, 0);
  
}
