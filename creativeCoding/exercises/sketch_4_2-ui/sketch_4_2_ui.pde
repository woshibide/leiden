Button[] buttons; // Define the array of buttons

void setup() {
  size(800, 800);
  buttons = new Button[4]; // Create an array to hold buttons
  for (int i = 0; i < 4; i++) {
    // Calculate the position of buttons
    float x, y;
    if (i == 0) {
      x = width / 2;
      y = height * 0.1;
    } else if (i == 1) {
      x = width * 0.1;
      y = height / 2;
    } else if (i == 2) {
      x = width / 2;
      y = height * 0.9;
    } else {
      x = width * 0.9;
      y = height / 2;
    }
    buttons[i] = new Button(x, y, width * 0.1, "Button " + (i + 1));
  }
}

void draw() {
  background(255);
  
  // Draw the central rectangular area
  float rectWidth = width * 0.7;
  float rectHeight = height * 0.7;
  rectMode(CENTER);
  fill(220);
  rect(width / 2, height / 2, rectWidth, rectHeight);
  
  // Display the buttons
  for (Button button : buttons) {
    button.display();
    
    // Check if the mouse is over a button
    if (button.isMouseOver()) {
      fill(150);
      rect(button.x, button.y, button.size, button.size);
    }
  }
}
