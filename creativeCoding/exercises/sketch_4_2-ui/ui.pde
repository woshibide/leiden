class Button {
  float x, y; // Position of the button
  float size; // Size of the button
  String label; // Label on the button
  boolean isHorizontal; // Is the button placed horizontally (true) or vertically (false)

  Button(float x, float y, float size, String label, boolean isHorizontal) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.label = label;
    this.isHorizontal = isHorizontal;
  }

  void display() {
    rectMode(CENTER);
    fill(200);
    rect(x, y, size, size);
    textAlign(CENTER, CENTER);
    fill(0);
    textSize(16);
    text(label, x, y);
  }

  boolean isMouseOver() {
    return mouseX >= x - size / 2 && mouseX <= x + size / 2 && mouseY >= y - size / 2 && mouseY <= y + size / 2;
  }
}
