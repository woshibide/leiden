import processing.sound.*;

Oscillator osc;

int hertz = 0;

float angle = 1, speed = 5, radius = 2000;
float sx = 50, sy = 30;
float  x1 = width / 2, y1 = height / 2, x2 = 10, y2 = 10;

void setup() {
  
  size(200, 200);
  background(100, 100, 100);
  osc = new SinOsc(this);
  fill(0,255,0);
  textAlign(CENTER);
  text("you are listening\ncomputer talking\nto a pigeon", width / 2, height / 2);
  
}

void update() {
    sx = cos(radians(mouseX));
    x1 = hertz + cos(radians(angle)) * radius;
    y1 = hertz + sin(radians(angle)) * radius;
    x2 = x1 + cos(radians(angle * sx)) * radius/2;
    y2 = y1 + sin(radians(angle * sy)) * radius/2;
    angle += speed;
  
}

void draw() {
  update();
  
  osc.freq(x2);
  osc.amp(sin(x1));
  update();
  osc.play();
  osc.freq(x1);
  
}
