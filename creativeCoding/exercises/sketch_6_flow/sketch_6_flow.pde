float inc = .001;
int scl = 5;
float zoff = 2;

int cols;
int rows;

int noOfPoints = 6000;

Particle[] particles = new Particle[noOfPoints];
PVector[] flowField;

void setup() {
  fullScreen();
  size(1000, 760, P2D);
  orientation(PORTRAIT);
  
  background(0, 0, 255);
  hint(DISABLE_DEPTH_MASK);
  
  cols = floor(width/scl);
  rows = floor(height/scl);
  
  flowField = new PVector[(cols*rows)];
  
  for(int i = 0; i < noOfPoints; i++) {
    particles[i] = new Particle();
  }
}

void draw() {
 fill(0,3);
 rect(0,0,width,height);
 //noFill();
  
  float yoff = 0;
  for(int y = 0; y < rows; y++) {
    float xoff = 0;
    for(int x = 0; x < cols; x++) {
      int index = (x + y * cols);

      float angle = noise(xoff, yoff, zoff) % TWO_PI;
      PVector v = PVector.fromAngle(angle);
      v.setMag(0.001);
      
      flowField[index] = v;
      
      stroke(18, 2);
      
      
       //pushMatrix();
       //stroke(250,75,150);
       //rect(x*scl*2, y*scl*4, 2, 2);
       //translate(x*scl*2, y%scl);
       //rotate(v.heading());
       //rect(0, 0, scl, 0);
      
       //popMatrix();
      
      xoff = xoff + inc;
    }
    yoff = yoff + inc;
  }
  zoff = zoff + (inc / 50);
  
  for(int i = 0; i < particles.length; i++) {
    particles[i].follow(flowField);
    particles[i].update();
    particles[i].edges();
    particles[i].show();
  }
}
