int d;  // Variable to control the size of shapes
int y;  // Variable to control the position of shapes
int s;  // Variable to control the rotation angle
float scaleFactor; // Variable to control the scaling factor
int recCounter; // Variable to control recursion depth

void setup() {
    fullScreen();
  background(0); // Set the background color
  frameRate(15);
  stroke(255,100); // Remove stroke from shapes

  d = width / 16;
  y = height / 2;
  s = d * 2;
  scaleFactor = 2;
  recCounter = 30;
}

void myLine(int recCounter) {
  text("hello", y, d);
  fill(map(y, 0, height, 0, 255), map(s, 0, 360, 0, 255));
  rect(d, y, d, y);
  d += 10; // Increase the size of the rectangle
  if (d > width) {
    d = int(random(10, 100)); // Randomize the size
    y = int(random(height)); // Randomize the position
  }
  recCounter--; // Reduce recursion depth
  if (recCounter > 0) {
    myLine(recCounter);
  }
}

void draw() {
  
  translate(width / 2, height / 2); // Center the drawing
  rotate(radians(s)); // Rotate based on 's'
  scaleFactor = radians(tan(frameCount * 0.2) * 0.8); // Sinusoidal scale factor
  scale(scaleFactor);
  rotate(s % 180); // Rotate based on 's'
  
  

  myLine(recCounter);
  

  s += 1; // Increase rotation angle
  if (s >= 40) {
    s = 0; // Reset the angle
  }
 
}
