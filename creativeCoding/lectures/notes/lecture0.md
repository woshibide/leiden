[sonic pi](https://sonic-pi.net/)

[pure data](https://software.puredata.info/docs)
[processing](https://processing.org/reference/)

# Creative Programming
making creative producs by writting software || programming as a medium for creative expression

## course info
- exercises every week to be done
- final outcome is a 'tool'

assessments
    [] homeworks assignments 50%
    [] programming projects 50%

## Intro to Processing

Its based of Java, simple dev environment for rapid prototyping

Color given as a single int (or 2 values, second for opacity) would infuence grayscale, meanwhile for rgb 3 values (4 including alpha) can be fed


### primitives
    arc()
    circle()
    ellipse()
    line()
    point()
    quad()
    rect()
    square()
    triangle()

 2D
    bezier(x1, y1, x2, y2, x3, y3, x4, y4)
    curve(x1, y1, x2, y2, x3, y3, x4, y4)

 3D
    bezier(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4) 
    curve(x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4) // Catmull-Rom Spline

    beginShape();
    vertex(x, y, z) // vertex actually takes in *4* values, assuming previous vertex as a starting point
    endShape(CLOSE); // to finish off the shape


### data types
int intValue = 5; // Integer (Number without decimals)
long longValue = 2147483648L; // "L" is added to number to mark it as a long
float floatValue = 1.12345; // Float (32-bit floating-point numbers)
double doubleValue = 1.12345D; // Double (64-bit floating-point numbers)

// NOTE!
// Although datatypes "long" and "double" work in the language,
// processing functions do not use these datatypes, therefore
// they need to be converted into "int" and "float" datatypes respectively,
// using (int) and (float) syntax before passing into a function



do loop // executes code and afterwards tests statement, if true turns into a while


## Randomness and Noise


if int(random(1) > 0.5) // a coin flip


### Noise space

// to produce a quirky circle
beginShape();
for int(i=0; i < 360; i += 10){
    cos(radians(angle))*radius;
    sin(radians(angle))*radius;
}
endShape(CLOSE);


to make drawn, noisy shape end in the starting point drawing needs to be in [the noise space]()

### random walk

float x = random(width);
float y
float s

x += random(20,80)

ellipse(x,y,s,s);

x = (x + width) % width;
y = (y + width) % width;