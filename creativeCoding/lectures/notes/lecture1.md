# Generational Programming

[Jared Tarbell](https://www.creativityfuse.com/2010/10/computer-art-jared-tarbells-otherworldly-digital-masterpieces/)

[Martin Wattenberg](https://www.moma.org/artists/33166#works)

[Casey Reas]()

[Martin Wattenberg]()

## Functions


different set of parameters can be stated via syntax like

void foo(int A, int B){};
void foo(int A, int B, float X, float Y);

so when calling foo with 2 parameters, first one will be executed. 4 values given on input will call second case of a function. this 

is particularly usefull when we want to prompt user to have a variability of input, providing a range of input

void foo(int A, int B){
     foo(A, B, float X, float Y);
};



declaring type of a function before calling it (or declaring, instead of void) is a good practice. Declaring instead of void 



an array can be fed into a function
When an array is passed to a function, the address (location in memory) of the array is given. So changes made within the function affect the array used as the parameter.


#### reflection
functions can containerise patterns that are to be applied onto anything (2d, 3d space and what not)



### Translation 

calling translate by itself shall move origin point (top left corner). it means all these manipulations are happening to the canvas 

### Rotation

rotate random(TWO_PI) // gives random rotation around 360 deg

### Scale

### Transformation Matrices

push and pop matrices

pushMatrix()
popMatrix()

basically undo / redo logic for canvas

pushMatrix();
translate(33, 0);
rect(0, 20, 50, 50)
popMatrix();
translate


### Styles
pushStyle(); // start a new style
popStyle(); // restore previous style

good practices is to indent


push();
pop();

works both for matrix and styles 



## Recursion

trees is a data type

commonly used to process tree-like structures

it is done via stack


void bar(int x, int foo);
if (foo==0) { return; } -- doesnt require function to have a return, but helps with earlier bail out

