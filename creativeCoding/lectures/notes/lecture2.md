# animation

## PImage

PImage[] images = new PImage[12]; // an array of frames can be fed into processing

images are best to be stored in 'data' folder, which is a default destination

image(img, x, y, width, height); // if image is smaller it gets to be stretched

### ordering

often used to format images/frames on input
[nf()](https://processing.org/reference/nf_.html)

### mapping functions

[norm()](https://processing.org/reference/norm_.html) // returns float in a 0.0 - 1.0 

[lerp()](https://processing.org/reference/lerp_.html) // linear interpolation,  returns
The lerp function is convenient for creating motion along a straight path and for drawing dotted lines.

[map()](https://processing.org/reference/map_.html)
[constrain()](https://processing.org/reference/constrain_.html)

norm() + lerp() = map()


syntax
map(value, start1, stop1, start2, stop2);


mapping functions helps to make sure that input range is always respected. restricts from leaving certain range values


norm(constrain(value, low, high), low, high); // 
lerp(low, high, constrain(value, 0, 1)); // 
map(constrain(value, low1, high1), low1, high1, low2, high2); //


norm(constrain(value, low, high). low. high) // to constrain on the input
constrain(norm(value,low, high), 0,1); // to not worry about input, rather just specify output


### time

millis(); returns number of mili seconds since the start of the program

good practice would be to call this function in setup, due to the jump inbetween program start and setup called

attachment to the time variable can enable developer to make sure no matter of the hardware users get to experience animation in the same manner. faster computers will just make animation smoother

recording start time of the function and its delta


### easing functions

type of interpolation to influence speed over time

[dist()](https://processing.org/reference/dist_.html)
dist(x1, y1, x2, y2);
dist(x1, y1, z1, x2, y2, z2);


they work in the same manner as map() function


lerp(easeIn());
lerp(easeOut());
lerp(easeInAndOut());


## motion

### organic motion 

// takes care of wrapping arround 
x = (x + width) % width;
y = (y + height) % height;


## states


in update() states of programm can be managed

push and pop is there to isolate functions / drawings


processing does push and pop arround each draw function 
