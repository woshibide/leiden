# interaction

## mouse
(pmouseX)[https://processing.org/reference/pmouseX.html]
(pmouseY)[https://processing.org/reference/pmouseY.html]


## keyboard
(key)[https://processing.org/reference/key.html]

key returns last pressed key as char, for "CODED" value it returns same stuff as in C#

key != keyPressed
keyPressed != keyReleased

keyPressed checks in a state of a program


"CODED" keys are 

## Polling vs Events

Polling constantly checks for the state of device. Mostly used for simple things

Events are notifications of state changes. Usually used when code base gets bigger

### Mouse Events
(mouseDragged)[https://processing.org/reference/mouseDragged_.html]
(mousePressed)[]
(mouseReleased)[]
(mouseClicked)[]
(mouseMoved)[]
(mouseWheel)[https://processing.org/reference/mouseWheel_.html]

```
void mouseMoved(){
    //
}
```


### Keyboard Events
keyTyped()

functions that handle events can be called at any time, so are not syncrhonised with draw() #glitchart

functions that handle events should insead change some state in your sketch 
    the updated state can the be used to control drawing the next time draw() is run


## OOP
абстракция короче
draw() inside a class doesnt add push and pop on each loop


### encapsulation 




### constructor
```
Balls = b;
b = new Balls(); // this is a moment called constructor. a new instantiation of an object
```

this.ClassName used to access vars inside a class

or reuse the same name with an underscore 


class
    this(_var, var, var)

## extra
instead of using for loops a list can be used ( via java.util list ) // generic programming

