# ideation

## process compendium 

[compendium](https://reas.com/compendium_text/)

The Elements, Forms, and Behaviors referenced within the Processes are defined in the Library:

Forms
F1: Circle
F2: Line


Behaviors
B1: Move in a straight line
B2: Constrain to surface
B3: Change direction while touching another Element
B4: Move away from an overlapping Element
B5: Enter from the opposite edge after moving off the surface
B6: Orient toward the direction of an Element that is touching
B7: Deviate from the current direction


Elements
E1: F1 + B1 + B2 + B3 + B4
E2: F1 + B1 + B5
E3: F2 + B1 + B3 + B5
E4: F1 + B1 + B2 + B3
E5: F2 + B1 + B5 + B6 + B7


## OOP 2

rationale behind it is it help humans make sense out of whats happening

### superclasses subclasses inheritance

has a relationship? - superclass 
is a relationship? - subclass

superclass shape
subclass   rectangle ellipse

tests tests tests tests tests tests tests tests tests tests tests tests 

### interfaces

there cannot be 2 super classes, meanwhile interfaces are for it


### abstract

abstract class Name {}



```
Imagine:

interface Domestic {}
class Animal {}
class Dog extends Animal implements Domestic {}
class Cat extends Animal implements Domestic {}

Imagine a dog object, created with Object dog = new Dog(), then:

dog instanceof Domestic // true - Dog implements Domestic
dog instanceof Animal   // true - Dog extends Animal
dog instanceof Dog      // true - Dog is Dog
dog instanceof Object   // true - Object is the parent type of all objects

However, with Object animal = new Animal();,

animal instanceof Dog // false

because Animal is a supertype of Dog and possibly less "refined".

And,

dog instanceof Cat // does not even compile!

This is because Dog is neither a subtype nor a supertype of Cat, and it also does not implement it.

Note that the variable used for dog above is of type Object. This is to show instanceof is a runtime operation and brings us to a/the use case: to react differently based upon an objects type at runtime.
```



## syntax 

<> lists can be linked or array

LinkedList // is more dynamic, works be keeping a notion of neighbouring elements 
ArrayList // when list exceeds its size a new one is added on the heap



daniel krooks