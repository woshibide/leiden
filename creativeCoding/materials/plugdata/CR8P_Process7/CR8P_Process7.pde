Element1[] elements;

void setup() {
  size(800, 600);
  elements = new Element1[400];
  for (int i = 0; i < elements.length; i++) {
    elements[i] = new Element1(random(width), random(height));
  }
}

void update() {
  for (Element1 element : elements) {
    element.update();
  }
}

void draw() {
  update();
  //background(255);
  stroke(0, 128);
  for (Element1 element : elements) {
    noFill();
    PVector p = element.position;
    float r = element.radius;
    //circle(p.x, p.y, element.radius*2);
    for (Element1 element2 : elements) {
      if (element.touching(element2)) {
        PVector p2 = element2.position;
        float r2 = element2.radius;
        float d = p.dist(p2);
        float g = map(d, 0, r + r2, 0, 255);
        stroke(g);
        line(p.x, p.y, p2.x, p2.y);
      }
    }
    stroke(0, 32);
    circle(p.x, p.y, 2*r);
    noStroke();
    fill(255, 128);
    circle(p.x, p.y, 2*r/100);
  }
}
