class Element1 {
  // E1: F1 + B1 + B2 + B3 + B4
  // F1: Circle
  // B1: Move in a straight line
  // B2: Constrain to surface
  // B3: Change direction while touching another Element
  // B4: Move away from an overlapping Element
  
  PVector position;
  float radius;
  
  float heading;
  float speed;
  
  Element1(float x, float y) {
    position = new PVector(x, y);
    heading = random(360);
    speed = width/400;
    radius = width/40;
  }
  
  boolean touching(Element1 other) {
    if (other == this) return false;
    float d = position.dist(other.position);
    return (d < radius + other.radius);
  }
  
  void update() {
    // B1: Move in a straight line
    PVector velocity =
      new PVector(cos(radians(heading)) * speed,
                  sin(radians(heading)) * speed);
    position.add(velocity);
    
    // B2: Constrain to surface
    if (position.x <= 0) { position.x = 0; }
    if (position.x >= width) { position.x = width; }
    if (position.y <= 0) { position.y = 0; }
    if (position.y >= height) { position.y = height; }
    
    for (Element1 element : elements) {
      if (element != this) {
        if (touching(element)) {
          // B3: Change direction while touching another Element
          heading += random(-1, 1);
          // B4: Move away from an overlapping Element
          PVector v = PVector.sub(position, element.position);
          v.mult(0.01);
          position.add(v);
        }
      }
    }
  }
}
