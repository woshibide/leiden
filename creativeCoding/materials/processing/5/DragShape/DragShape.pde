PGraphics offscreen;
boolean dragging = false;
float dragX, dragY, dragW, dragH;

void setup() {
  size(400, 400);
  offscreen = createGraphics(width, height);
}

void draw() {
  background(255);
  image(offscreen, 0, 0);
  if (dragging) {
    noFill();
    stroke(0, 0, 200, 100);
    rect(dragX, dragY, dragW, dragH);
  }
}

void mousePressed() {
  dragX = mouseX; dragY = mouseY;
  dragging = false;
}

void mouseDragged() {
  dragW = mouseX-dragX; dragH = mouseY-dragY;
  dragging = true;
}

void mouseReleased() {
  dragging = false;
  offscreen.beginDraw();
  offscreen.noFill();
  offscreen.strokeWeight(2);
  offscreen.rect(dragX, dragY, dragW, dragH);
  offscreen.endDraw();
}
