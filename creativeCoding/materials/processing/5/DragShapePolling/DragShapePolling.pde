PGraphics offscreen;
boolean dragging = false;
float dragX, dragY, dragW, dragH;

void setup() {
  size(400, 400);
  noFill();
  stroke(0, 0, 200, 100);
  offscreen = createGraphics(400, 400);
}

void update() {
  if (dragging) {
    if (mousePressed) {
      dragW = mouseX - dragX;
      dragH = mouseY - dragY;
    } else {
      dragging = false;
      offscreen.beginDraw();
      offscreen.noFill();
      offscreen.strokeWeight(2);
      offscreen.rect(dragX, dragY, dragW, dragH);
      offscreen.endDraw();
    }
  } else {
    if (mousePressed) {
      dragX = mouseX;
      dragY = mouseY;
      dragW = 0;
      dragH = 0;
      dragging = true;
    }
  }
}

void draw() {
  update();
  background(255);
  image(offscreen, 0, 0);
  if (dragging) {
    rect(dragX, dragY, dragW, dragH);
  }
}
