static class Linear {
  static float easeIn(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : t/d;
  }
  
  static float easeOut(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : t/d;
  }
  
  static float easeInOut(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : t/d;
  }  
}

static class Quad {
  static float easeIn(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    return t*t;
  }
  
  static float easeOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    return (2*t) - (t*t);
  }
  
  static float easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Cubic {
  static float easeIn(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = (t/d);
    return t*t*t;
  }
  
  static float easeOut (float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = (t/d) - 1;
    return 1 + t*t*t;
  }
  
  static float easeInOut (float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Quart {
  static float easeIn(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    return t*t*t*t;
  }
  
  static float easeOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d - 1;
    return 1 - t*t*t*t;
  }
  
  static float easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Quint {
  static float easeIn(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    return t*t*t*t*t;
  }
  
  static float easeOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d - 1;
    return 1 + t*t*t*t*t;
  }
  
  static float easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Sine {
  static float easeIn(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : 1 - cos(t/d * (PI/2));
  }
  
  static float easeOut(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : sin(t/d * (PI/2));  
  }
  
  static float easeInOut(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : 0.5 * (1 - cos(t/d * PI));
  }
}

static class Circ {
  static float easeIn(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    return 1 - sqrt(1 - t*t);
  }
  
  public static float  easeOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d - 1;
    return sqrt(1 - t*t);
  }
  
  public static float  easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Expo {
  static float easeIn(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : pow(2, 10 * (t/d - 1));
  }
  
  static float  easeOut(float t, float d) {
    return (t <= 0) ? 0 : (t >= d) ? 1 : 1 - pow(2, -10 * t/d);  
  }
  
  public static float  easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Back {
  static float easeIn(float t, float d) {
    return Back.easeIn(t, d, 1.70158f);
  }
  
  static float easeIn(float t, float d, float s) {
    float i = t/d;
    return (t <= 0) ? 0 : (t >= d) ? 1 : i*i*((s+1)*i - s);
  }
  
  static float easeOut(float t, float d) {
    return Back.easeOut(t, d, 1.70158f);
  }

  static float easeOut(float t, float d, float s) {
    float i = t/d - 1;
    return (t <= 0) ? 0 : (t >= d) ? 1 : 1 + i*i*((s+1)*i + s);
  }
  
  static float easeInOut(float t, float d) {
    return Back.easeInOut(t, d, 1.70158f);
  }
  
  static float easeInOut(float t, float d, float s) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d, s);
    else return 0.5 * (1 + easeOut(t*2-d, d, s));
  }
}

static class Bounce {
  static float easeIn(float t, float d) {
    return 1 - easeOut(d-t, d);
  }
  
  static float easeOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    if (t < (1/2.75f)) {
      return (t*t*7.5625f);
    } else if (t < (2/2.75f)) {
      t -= (1.5f/2.75f);
      return (t*t*7.5625f + 0.75f);
    } else if (t < (2.5/2.75)) {
      t -= (2.25f/2.75f);
      return (t*t*7.5625f + 0.9375f);
    } else {
      t -= (2.625f/2.75f);
      return (t*t*7.5625f + 0.984375f);
    }
  }
  
  static float easeInOut(float t, float d) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    if (t < d/2) return 0.5 * easeIn(t*2, d);
    else return 0.5 * (1 + easeOut(t*2-d, d));
  }
}

static class Elastic {
  static float easeIn(float t, float d) {
    return Elastic.easeIn(t, d, 1, d * 0.3);
  }

  static float easeIn(float t, float d, float a, float p) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    float s;
    if (a < 1) { a = 1;  s = p/4; }
    else { s = p/(2*PI) * asin(1/a); }
    t -= 1;
    return -(a*pow(2, 10*t) * sin((t*d-s) * (2*PI)/p));
  }

  static float easeOut(float t, float d) {
    return Elastic.easeOut(t, d, 1, d * 0.3);
  }
  
  static float easeOut(float t, float d, float a, float p) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/d;
    float s;
    if (a < 1) { a = 1;  s = p/4; }
    else { s = p/(2*PI) * asin(1/a); }
    return (a*pow(2,-10*t) * sin((t*d-s)*(2*PI)/p) + 1);  
  }
  
  static float easeInOut(float t, float d) {
    return Elastic.easeInOut(t, d, 1, d * 0.3 * 1.5);
  }
  
  static float easeInOut(float t, float d, float a, float p) {
    if (t <= 0) return 0;
    if (t >= d) return 1;
    t = t/(d/2);
    float s;
    if (a < 1) { a = 1; s = p/4; }
    else { s = p/(2*PI) * asin(1/a); }
    t -= 1;
    if (t < 0) return -0.5 * (a*pow(2,10*t) * sin((t*d-s)*(2*PI)/p));
    return a*pow(2,-10*t) * sin((t*d-s)*(2*PI)/p) * 0.5 + 1;
  }
}
