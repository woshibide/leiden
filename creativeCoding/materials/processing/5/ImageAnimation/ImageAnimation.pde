// Photo by <a href="https://unsplash.com/@karsten116?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Karsten Winegeart</a> on <a href="https://unsplash.com/photos/NE0XGVKTmcA?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>

PImage img;
float x, y, scale, rotation, opacity;
int state = 0;
int duration = 0;
int startMillis;

void setup() {
  size(400, 400);
  img = loadImage("puppy.jpg");
  imageMode(CENTER);
  x = width/2;
  y = height/2;
  scale = 0.5;
  rotation = 0;
  opacity = 0;
  setState(0, 2000);
}

void setState(int _state, int _duration) {
  state = _state;
  duration = _duration;
  startMillis = millis();
}

void update() {
  int t = millis() - startMillis;
  if (state == 0) {
    if (t < duration) {
      scale = 0.2 + 0.6 * Bounce.easeOut(t, duration);
      opacity = 255 * Bounce.easeOut(t, duration);
    } else {
      setState(1, 2000);
    }
  } else if (state == 1) {
    if (t < duration) {
      rotation = 180 * Back.easeIn(t, duration);
    } else {
      setState(2, 3000);
    }
  } else if (state == 2) {
    if (t < duration) {
      rotation = 180 + 180 * Elastic.easeOut(t, duration);
    } else {
      setState(3, 2000);
    }
  } else if (state == 3) {
    if (t < duration) {
      scale = 0.2 + 0.6 * (1 - Bounce.easeOut(t, duration));
      opacity = 255 * (1 - Bounce.easeOut(t, duration));
    } else {
      setState(0, 2000);
    }
  }
}

void draw() {
  update();
  background(255);
  translate(x, y);
  rotate(radians(rotation));
  scale(scale);
  tint(255, opacity);
  image(img, 0, 0, width, height);
}
