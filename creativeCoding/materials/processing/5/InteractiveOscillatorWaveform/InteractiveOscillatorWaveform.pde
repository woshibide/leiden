import processing.sound.*;

Oscillator osc;
Waveform wave;

void setup() {
  size(400, 400);
  osc = new SinOsc(this);
  wave = new Waveform(this, width);
  wave.input(osc);
  osc.play();
}

void draw() {
  osc.amp(map(mouseY, 0, height, 1, 0));
  osc.freq(map(mouseX, 0, width, 80, 1000));
  wave.analyze();
  background(0);
  stroke(255);
  strokeWeight(2);
  noFill();
  beginShape();
  for(int x = 0; x < width; x++) {
    float y = map(wave.data[x], 1, -1, 0, height);
    vertex(x, y);
  }
  endShape();
}
