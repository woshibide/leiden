PGraphics frontPlate;
PGraphics guageIndicator;
PGraphics guageMask;
PGraphics guageOverlay;
int gw, gh;

void setup() {
  size(1100, 220);
  pixelDensity(2);
  gw = 200;
  gh = 200;
  frontPlate = createGraphics(gw, gh);
  guageIndicator = createGraphics(gw, gh);
  guageMask = createGraphics(gw, gh);
  guageOverlay = createGraphics(gw, gh);
  drawGuageMask();
  drawFrontPlate();
}

float deltaAngle = 2;
float guageAngle = 0;
boolean guageArmed = false;
boolean showLayers = true;

void update() {
  if (!guageArmed) {
    guageAngle += deltaAngle;
    if (guageAngle >= 360 || guageAngle <= 0) {
      deltaAngle = -deltaAngle;
      guageAngle += deltaAngle;
    }
  }
  updateGuageIndicator(guageAngle);
  updateGuageOverlay(guageAngle);
}

void mousePressed() {
  if (mouseX < 220 && mouseY < 220) {
    guageArmed = true;
    float dx = mouseX - (gw/2 + 10);
    float dy = mouseY - (gh/2 + 10);
    guageAngle = (degrees(atan2(dy, dx)) + 360 + 90) % 360;
  }
}

void mouseDragged() {
  float dx = mouseX - (gw/2 + 10);
  float dy = mouseY - (gh/2 + 10);
  guageAngle = (degrees(atan2(dy, dx)) + 360 + 90) % 360;
}

void mouseReleased() {
  guageArmed = false;
}

void keyPressed() {
  showLayers = !showLayers;
}

void draw() {
  update();
  background(64);
  // show layers
  if (showLayers) {
    image(frontPlate, 230, 10);
    image(guageMask, 450, 10);
    image(guageIndicator, 670, 10);
    image(guageOverlay, 890, 10);
  }
  guageIndicator.mask(guageMask);
  if (guageArmed) {
    guageIndicator.filter(BLUR, 2);
  }
  // draw layers
  image(frontPlate, 10, 10);
  image(guageIndicator, 10, 10);
  image(guageOverlay, 10, 10);
}

void drawGuageMask() {
  guageMask.beginDraw();
    guageMask.fill(0,0,0,0);      // transparent (ellipse is ring, not circle)
    guageMask.stroke(255);        // white (mask area)
    guageMask.strokeWeight(20);   // ring thickness
    guageMask.ellipse(gw/2, gh/2, gw/2, gh/2);  // draw ring
  guageMask.endDraw();
}

void drawFrontPlate() {
  frontPlate.beginDraw();
  frontPlate.push();
  frontPlate.fill(100);
  frontPlate.noStroke();
  frontPlate.rect(0, 0, gw, gh, 10);
  frontPlate.noFill();
  frontPlate.stroke(0, 64);
  frontPlate.strokeWeight(22);
  frontPlate.ellipse(gw/2, gh/2, gw/2, gh/2);
  frontPlate.pop();
  frontPlate.push();
  frontPlate.noFill();
  frontPlate.stroke(255, 64);
  frontPlate.translate(gw/2, gh/2);
  frontPlate.rotate(radians(-90));
  float inner = 0.65*gw/2;
  float minor = inner + 5;
  float major = inner + 10;
  frontPlate.circle(0, 0, 2*inner);
  push();
  for (int m = 0; m < 360; m += 5) {
    if (m % 30 == 0) {
      frontPlate.line(inner, 0, major, 0);
    } else {
      frontPlate.line(inner, 0, minor, 0);
    }
    frontPlate.rotate(radians(5));
  }
  pop();
  frontPlate.noStroke();
  frontPlate.fill(255, 128);
  frontPlate.textSize(8);
  frontPlate.textAlign(CENTER, CENTER);
  for (int m = 0; m < 360; m += 30) {
    frontPlate.push();
    frontPlate.rotate(radians(m));
    frontPlate.translate(major + 10, 0);
    frontPlate.rotate(radians(90));
    frontPlate.text(m, 0, 0);
    frontPlate.pop();
  }  
  frontPlate.pop();
  frontPlate.endDraw();
}

void updateGuageIndicator(float a) {
  guageIndicator.beginDraw();
  float gd = gw*0.75;
  guageIndicator.translate(gw/2, gh/2);
  guageIndicator.rotate(radians(-90));
  guageIndicator.colorMode(HSB, 360, 100, 100, 100);
  guageIndicator.strokeWeight(3);
  for (float h = 0; h < 360; h += 2) {
    if ((h+1) < a) {
      if (guageArmed) {
        guageIndicator.stroke(h, 100, 80);
      } else {
        guageIndicator.stroke(h, 100, 50);
      }
    } else {
      guageIndicator.stroke(h, 100, 20);
    }
    guageIndicator.pushMatrix();
    guageIndicator.rotate(radians(h));
    guageIndicator.line(0, 0, gd/2, 0);
    guageIndicator.popMatrix();
  }
  guageIndicator.endDraw();
}

void updateGuageOverlay(float a) {
  float inner = 0.65*gw/2;
  float major = inner + 10;
  float midline = (inner + major)/2;

  guageOverlay.beginDraw();
  guageOverlay.clear();

  guageOverlay.translate(gw/2, gh/2);
  guageOverlay.rotate(radians(-90));
  
  guageOverlay.push();
  guageOverlay.noFill();
  if (guageArmed) {
    guageOverlay.stroke(255, 192);
  } else {
    guageOverlay.stroke(255, 128);
  }
  guageOverlay.arc(0, 0, 2*inner, 2*inner, 0, radians(a));
  guageOverlay.line(inner, 0, major, 0);
  guageOverlay.rotate(radians(a));
  guageOverlay.line(inner, 0, major, 0);
  guageOverlay.line(0, 0, inner/2, 0);
  guageOverlay.pop();

  guageOverlay.push();
  guageOverlay.noFill();
  if (guageArmed) {
    guageOverlay.stroke(255, 64);
  } else {
    guageOverlay.stroke(255, 32);
  }
  guageOverlay.strokeWeight(major - inner);
  guageOverlay.strokeCap(SQUARE);
  guageOverlay.arc(0, 0, 2*midline, 2*midline, 0, radians(a));
  guageOverlay.pop();
  
  guageOverlay.endDraw();
}
