PGraphics trail, smoke;
float x, y, px, py;

void setup() {
  size(400, 400);
  trail = createLayer(width, height, #88FFFFFF);
  smoke = createLayer(width/4, height/4, #FF0000);
  colorMode(HSB, 360, 100, 100, 100);
  update();
}

PGraphics createLayer(int w, int h, color c) {
  PGraphics pg = createGraphics(w, h);
  pg.beginDraw();
  pg.background(0);
  pg.stroke(c);
  pg.endDraw();
  return pg;
}

void update() {
  px = x; py = y;
  x = width * noise(millis() * 0.0017);
  y = height * noise(millis() * 0.0011);
}

void draw() {
  update();
  drawSmoke(smoke);
  drawTrail(trail);
  background(frameCount%360, 100, 20);
  // Use additive blending to composite
  blend(trail, 0, 0, width, height,
               0, 0, width, height, ADD);
  blend(smoke, 0, 0, width/4, height/4,
               0, 0, width, height, ADD);
}

void drawTrail(PGraphics pg) {
  pg.beginDraw();
  pg.line(px, py, x, y);
  pg.endDraw();
}

void drawSmoke(PGraphics pg) {
  pg.beginDraw();
  pg.ellipse(x/4, y/4, 5, 5);
  pg.endDraw();
  pg.filter(BLUR);
}
