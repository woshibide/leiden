PGraphics background;
PGraphics shadow;
PGraphics mask1;
PGraphics foreground1;
PGraphics mask2;
PGraphics foreground2;

PImage[] layers;
String[] labels;

boolean showMinimap = false;

void setup() {
  fullScreen();
  //size(800, 600);
  
  background = createGraphics(width, height);
  shadow = createGraphics(width, height);
  mask1 = createGraphics(width, height);
  mask2 = createGraphics(width, height);
  foreground1 = createGraphics(width, height);
  foreground2 = createGraphics(width, height);

  layers = new PImage[]{ background, foreground1, foreground2, mask1, mask2, shadow };
  labels = new String[]{ "background", "foreground1", "foreground2", "mask1", "mask2", "shadow" };
  
  background.beginDraw();
  background.background(255);
  background.endDraw();

  shadow.beginDraw();
  shadow.translate(width/2, height/2);
  shadow.noFill();
  shadow.stroke(0, 128);
  shadow.strokeWeight(width/8);
  shadow.circle(10, 10, 2*height/3);
  shadow.strokeWeight(width/16);
  shadow.circle(10, 10, 2*height/8);
  shadow.filter(BLUR, 10);
  shadow.endDraw();

  mask1.beginDraw();
  mask1.translate(width/2, height/2);
  mask1.noFill();
  mask1.stroke(255);
  mask1.strokeWeight(width/8);
  mask1.circle(0, 0, 2*height/3);
  mask1.endDraw();

  mask2.beginDraw();
  mask2.translate(width/2, height/2);
  mask2.noFill();
  mask2.stroke(255);
  mask2.strokeWeight(width/16);
  mask2.circle(0, 0, 2*height/8);
  mask2.endDraw();
}

void draw() {
  drawPattern(background, color(0, 100, 200), frameCount, 1);
  drawPattern(foreground1, color(200, 0, 100), frameCount, 1.03);
  drawPattern(foreground2, color(0, 200, 100), frameCount, 1.1);
  image(background, 0, 0);
  
  image(shadow, 0, 0);
  foreground1.mask(mask1);
  image(foreground1, 0, 0);
  foreground2.mask(mask2);
  image(foreground2, 0, 0);
  
  // If a key has been pressed draw "minimap" of layers over the background
  if (showMinimap) {
    drawMinimap(layers, labels );
  }
}

void keyPressed() {
  showMinimap = !showMinimap;
}

void drawMinimap(PImage[] images, String[] labels) {
  push();
  scale(0.1);
  fill(64, 64);
  textSize(height/5);
  translate(40, 40);
  float w = 2 * 40 + width;
  float h = 2 * 40 + images.length * (height + 160);
  rect(0, 0, w, h, 40);
  translate(40, 40);
  for (int i = 0; i < images.length; i++) {
    drawMinimapImage(images[i], labels[i]);
    translate(0, 160 + height);
  }
  pop();
}

void drawMinimapImage(PImage image, String label) {
  fill(64, 64);
  rect(0, 0, width, height);
  image(image, 0, 0);
  pushStyle();
  fill(0, 200);
  text(label, 20, height + 100);
  popStyle();
}

void drawPattern(PGraphics pg, color c, int f, float sc) {
  pg.beginDraw();
  pg.background(255);
  pg.push();
  pg.noStroke();
  pg.fill(c, 64);
  pg.translate(width/2, height/2);
  pg.scale(sc);
  float s = map(noise(f * 0.003), 0.2, 0.8, 0.6, 1.0);
  pg.scale(s);
  float a = map(noise(f * 0.005), 0.2, 0.8, 0, 90);
  pg.rotate(radians(a));
  for (int i = 0; i < 200; i++) {
    float x = map(noise(i*17 + f * 0.00031), 0.2, 0.8, -width, width);
    float y = map(noise(i*19 + f * 0.00037), 0.2, 0.8, -width, width);
    float r = width/20 * noise(i*23 + f * 0.013);
    pg.circle(x, y, 2*r);
  }
  pg.pop();
  pg.endDraw();
}
