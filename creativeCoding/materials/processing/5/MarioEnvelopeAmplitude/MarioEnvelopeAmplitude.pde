import processing.sound.*;

Oscillator osc;
Env env;
Amplitude amp;

float response = 0;
float smoothing = 0.8;

float attack = 0.001;  // time in seconds
float sustain = 0.004; // time in seconds
float level = 0.3;     // amplitude
float release = 0.2;   // time in seconds

int[] midiNotes = {76,76,0,76,0,72,76,0,79,0,67,0};
int[] durations = { 2, 2,2, 2,2, 2, 2,2, 2,6, 4,4};
int noteIndex = 0;

int trigger;

void setup() {
  size(400, 400);
  background(255);
  osc = new TriOsc(this);
  env = new Env(this);
  amp = new Amplitude(this);
  amp.input(osc);
  trigger = millis() + 200;
}

void update() {
  if ((millis() > trigger)) {
    int note = midiNotes[noteIndex];
    int duration = durations[noteIndex];
    if (note != 0) {
      osc.play(midiToFreq(note), 0.8);
      env.play(osc, attack, sustain, level, release);
    }
    trigger = millis() + (duration * 73);
    noteIndex = (noteIndex + 1) % midiNotes.length;
  }
  response += (amp.analyze() - response) * smoothing;
}

void draw() {
  update();
  background(#08A936);
  fill(#E0102F);
  stroke(255);
  strokeWeight(4);
  float radius = height/8 + response * height;
  ellipse(width/2, height/2, radius, radius);
}

float midiToFreq(int note) {
  return (pow(2, ((note-69)/12.0))) * 440;
}
