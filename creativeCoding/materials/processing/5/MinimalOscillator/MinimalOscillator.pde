import processing.sound.*;

Oscillator osc;

int hertz = 0;

float angle = 90, speed = 2, radius = 2000;
float sx = 50, sy = 30;
float  x1 = width / 2, y1 = height / 4, x2 = 0, y2 = 0;

void setup() {
  
  size(200, 200);
  osc = new SinOsc(this);
  
}

void update() {

    x1 = hertz + cos(radians(angle)) * radius;
    y1 = hertz + sin(radians(angle)) * radius;
    x2 = x1 + cos(radians(angle * sx)) * radius/2;
    y2 = y1 + sin(radians(angle * sy)) * radius/2;
    angle += speed;
  
}

void draw() {
  update();
  osc.amp(0.5);
  osc.freq(x2);
  osc.play();
}
