import processing.sound.*;

SoundFile sample;

void setup() {
  size(400, 400);
  sample = new SoundFile(this, "xylophon.mp3");
  sample.play();
}

void draw() {}
