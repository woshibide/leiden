import processing.sound.*;

SoundFile sample;
Waveform wave;

void setup() {
  size(400, 400);
  frameRate(60);
  sample = new SoundFile(this, "xylophon.mp3");
  wave = new Waveform(this, width);
  wave.input(sample);
  sample.play();
}

void draw() {
  wave.analyze();
  background(0);
  stroke(255);
  strokeWeight(2);
  noFill();
  beginShape();
  for(int x = 0; x < width; x++) {
    float y = map(wave.data[x], 1, -1, 0, height);
    vertex(x, y);
  }
  endShape();
}
