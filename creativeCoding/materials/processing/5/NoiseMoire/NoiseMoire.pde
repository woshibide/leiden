int w, h;

void setup() {
  size(800, 800);
  textSize(40);
  w = width; h = height;
}

void draw() {
  background(255);
  float m = millis() * 0.00001;
  int[] n = {int(w*(noise(17+11*m))), int(w*(noise(11+19*m))),
             int(w*(noise(19+71*m))), int(w*(noise(71+17*m)))};
  noFill();
  stroke(0, 160);
  for (int i = 12; i < 4*w; i += 12) {
    for (int j = 0; j < n.length; j++) {
      circle(n[j], n[(j+1)%n.length], i);
    }
  }
  filter(BLUR);
  fill(200, 0, 0);
  text(frameRate, 5, height - 5);
}
