PGraphics g;
int w, h;

void setup() {
  size(800, 800);
  textSize(40);
  w = width; h = height;
  g = createGraphics(2*w, 2*h);
  g.beginDraw();
  g.noFill();
  g.stroke(0, 200);
  for (int i = 12; i < 4*w; i += 12) {
    g.circle(w,h,i);
  }
  g.endDraw();
  g.filter(BLUR);
}

void draw() {
  background(255);
  float m = millis() * 0.00001;
  int[] n = { int(w*(noise(17+11*m)-1)),
              int(w*(noise(11+19*m)-1)),
              int(w*(noise(19+71*m)-1)),
              int(w*(noise(71+17*m)-1)) };
  for (int j = 0; j < n.length; j++) {
    copy(g, 0, 0, 2*w, 2*h,
            n[j], n[(j+1)%n.length], 2*w, 2*h);
  }
  fill(200, 0, 0); text(frameRate, 5, height - 5);
}
