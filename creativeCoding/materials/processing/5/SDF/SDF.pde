void setup() {
  size(400, 400);
  colorMode(RGB, 1, 1, 1, 1);
}

void draw() {
  PVector p = new PVector();
  PVector p1 = new PVector();
  PVector p2 = new PVector();
  PVector b = new PVector();
  loadPixels();
  for (int i = 0; i < pixels.length; i++) {
    p.x = 2 * float(i % width)/width - 1;
    p.y = 2 * float(i / width)/height - 1;
    p.rotate(PI * millis() * 0.0001);
    p1 = p1.set(p.x, p.y).rotate(PI/4);
    p2 = p2.set(p.x, p.y).rotate(-PI/4);
    float t = millis()/2000.0;
    float r = 0.8 + 0.02 * sin(t * TWO_PI);
    // float d = sdCircle(x, y, r);
    b.x = r; b.y = r/6;
    float d =
      opRound(
        opOnion(
          min(sdBox(p1, b), sdBox(p2, b)),
          0.05),
      0.01);
    float g = 0;
    color c = color(g);
    if (abs(d) < 0.01) {
      g = 1;
      c = color(g);
    } else if (d < 0) {
      g = 0.6 * abs(2*d) * (cos((19*d) * TWO_PI)+1)/2 + 0.4;
      c = color(0.6*g, 0.8*g, g);
    } else if (d > 0) {
      g = 0.6 * abs(2*d) * (cos((19*d) * TWO_PI)+1)/2 + 0.4;
      c = color(g, 0.7*g, 0.5*g);
    }
    pixels[i] = c;
  }
  updatePixels();
}

float opRound(float d, float r) {
  return d - r;
}

float opOnion(float d, float r) {
  return abs(d) - r;
}

float sdCircle(PVector p, float radius) {
  return p.mag() - radius;
}

float sdBox(PVector p, PVector b) {
  float dx = abs(p.x) - b.x;
  float dy = abs(p.y) - b.y;
  return sqrt(sq(max(dx, 0)) + sq(max(dy, 0))) + min(max(dx, dy), 0);
}

PVector abs(PVector v) {
  v.set(abs(v.x), abs(v.y)); return v;
}

PVector max(PVector v, float s) {
  v.set(max(v.x, s), max(v.y, s)); return v;
}
