class Ball {
  float radius;
  Box bounds;
  PVector position;
  PVector velocity;
  boolean collision;
  float redness = 0;
  
  Ball(float _radius, Box _bounds) {
    bounds = _bounds;
    radius = _radius;
    respawn();
  }
  
  void respawn() {
    do { // A cheeky do...while to make sure position is set
      collision = false;
      position = new PVector(bounds.x + random(radius, bounds.width - radius),
                             bounds.y + random(radius, bounds.height - radius));
      for (int i = 0; i < balls.length; i++) {
        Ball b = balls[i];
        if (b == null) break;
        if (b != this) {
          PVector n = PVector.sub(position, b.position);
          if (n.mag() < radius + b.radius) {
            collision = true;
          }
        }
      }
    } while (collision);
    float heading = random(TWO_PI);
    float speed = random(radius/16, radius/8);
    velocity = new PVector(cos(heading) * speed,
                           sin(heading) * speed);
  }
  
  void update() {
    position.x += velocity.x;
    position.y += velocity.y;
    
    if (position.x < bounds.x + radius) {
      position.x = bounds.x + radius;
      velocity.x = -velocity.x;
    } else if (position.x > bounds.x + bounds.width - radius) {
      position.x = bounds.x + bounds.width - radius;
      velocity.x = -velocity.x;
    }
    
    if (position.y < bounds.y + radius) {
      position.y = bounds.y + radius;
      velocity.y = -velocity.y;
    } else if (position.y > bounds.y + bounds.height - radius) {
      position.y = bounds.y + bounds.height - radius;
      velocity.y = -velocity.y;
    }
    
    collision = false;
  }

  void draw() {
    push();
    if (collision) {
      redness = 200;
    } else {
      redness *= 0.9;
    }
    fill(redness, 0, 0);
    noStroke();
    ellipseMode(CENTER);
    translate(position.x, position.y);
    circle(0, 0, 2*radius);
    pop();
  }
}
