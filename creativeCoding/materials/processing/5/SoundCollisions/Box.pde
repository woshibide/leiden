class Box {
  float x, y, width, height;
  
  Box(float _x, float _y, float _width, float _height) {
    x = _x; y = _y; width = _width; height = _height;
  }
  
  void draw() {
    push();
    stroke(0, 32);
    noFill();
    rect(x, y, width, height);
    pop();
  }
}
