import processing.sound.*;

Box box;
Ball[] balls;
SoundFile[] soundfiles;

int numBalls;

void setup() {
  size(400, 400);
  
  box = new Box(0, 0, 400, 400);  
  balls = new Ball[60];
  soundfiles = new SoundFile[balls.length];
  
  for (int i = 0; i < balls.length; i++) {
    balls[i] = new Ball(random(box.width * 0.02, box.width * 0.04), box);
    soundfiles[i] = new SoundFile(this, "sfx_sounds_impact" + (1+i%8) + ".wav");
  }

  numBalls = 2;
}

void update() {
  for (int i = 0; i < numBalls; i++) {
    balls[i].update();
  }

  for (int i = 0; i < numBalls-1; i++) {
    Ball b1 = balls[i];
    for (int j = i+1; j < numBalls; j++) {
      Ball b2 = balls[j];
      PVector n = PVector.sub(b1.position, b2.position);
      if (n.mag() <= (b1.radius + b2.radius)) {
        b1.collision = b1.collision || true;
        b2.collision = b2.collision || true;
        n.normalize();
        PVector c = PVector.add(b1.position, b2.position).mult(0.5);
        b1.position = PVector.add(c, PVector.mult(n, b1.radius));
        b2.position = PVector.sub(c, PVector.mult(n, b2.radius));
        float a1 = PVector.dot(b1.velocity, n);
        float a2 = PVector.dot(b2.velocity, n);
        float op = (2 * (a1 - a2)) / 2;
        n.mult(op);
        b1.velocity.sub(n);
        b2.velocity.add(n);
      }
    }
  }
  
  for (int i = 0; i < numBalls; i++) {
    if (balls[i].collision) {
      soundfiles[i].stop();
      soundfiles[i].play();
    }
  }
}

void draw() {
  update();
  if (numBalls < balls.length) {
    background(255);
  } else {
    background(240, 180, 0);
  }
  box.draw();
  for (int i = 0; i < numBalls; i++) {
    balls[i].draw();
  }
}

void mouseClicked() {
  if (mouseButton == LEFT && numBalls < balls.length) {
    balls[numBalls].respawn();
    numBalls++;
  } else if (mouseButton == RIGHT && numBalls > 2) {
    numBalls--;
  }
}

void keyReleased() {
  if (key == CODED) {
    if (keyCode == UP && numBalls < balls.length) {
      balls[numBalls].respawn();
      numBalls++;
    } else if (keyCode == DOWN && numBalls > 2) {
      numBalls--;
    }
  }
}
