void setup() {
  size(400, 400);
  colorMode(RGB, 1, 1, 1);
}

void draw() {
  float x0 = frameCount / 10.0;
  float t = millis() / 10000.0;
  PVector p = new PVector();
  loadPixels();
  for (int i = 0; i < pixels.length; i++) {
    float x = 2 * float(i % width)/width - 1;
    float y = 2 * float(i / width)/height - 1;
    float d = dist(x, y, 0, 0) * 0.3;
    float s = sin(t*TWO_PI);
    p.set(x, y).mult(2 + s).rotate(d*TWO_PI);
    float g1 = map(noise(100+p.x, 11*t+p.y), 0.2, 0.8, 0, 1);
    float g2 = map(noise(200+p.x, 17*t+p.y), 0.2, 0.8, 0, 1);
    float g3 = map(noise(300+p.x, 29*t+p.y), 0.2, 0.8, 0, 1);
    color c = color(round(3 * g1)/3.0,
                    round(3 * g2)/3.0,
                    round(3 * g3)/3.0);
    pixels[i] = c;
  }
  updatePixels();
  
  // Uncomment these lines to display the frameRate
  //String s = nf(int(frameRate));
  //noStroke();
  //fill(0, 100);
  //rect(0, height, textWidth(s) + 10, -25);
  //fill(0, 200, 0);
  //textSize(20);
  //text(s, 5, height-5);
}
