PFont font;

void setup() {
  size(400, 400);
  font = createFont("ChunkFive-Regular.otf", 64);
  textFont(font);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  translate(width/2, height/2);
  fill(192, 0, 0);
  rotate(radians(random(-10, 10)));
  text("ANGRY", random(-10, 10), random(-10, 10));
}
