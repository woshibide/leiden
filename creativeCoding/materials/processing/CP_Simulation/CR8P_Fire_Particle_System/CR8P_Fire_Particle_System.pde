ParticleSystem fire;
PImage fire_img;

void setup() {
  size(400, 400);
  fire = new ParticleSystem(width/2, height/2, 100);
  fire_img = loadImage("fire_particle.png");
  if (fire_img == null) {
    generateImages();
    fire_img = loadImage("fire_particle.png");
  }
}

void draw() {
  fire.update();
  background(100);
  imageMode(CENTER);
  for (int i = 0; i < fire.particles.length; i++) {
    PVector p = fire.particles[i].position;
    int x = int(p.x) - fire_img.width/2;
    int y = int(p.y) - fire_img.height/2;
    blend(fire_img, 0, 0, fire_img.width, fire_img.height,
                    x, y, fire_img.width, fire_img.height, ADD);
  }
}

void generateImages() {
  generateFireParticleImage();
}

void generateFireParticleImage() {
  int radius = 20;
  PImage img = createImage(2*radius, 2*radius, ARGB);
  for (int i = 0; i < img.width; i++) {
    for (int j = 0; j < img.height; j++) {
      float d = dist(radius, radius, i, j);
      float a = map(d, 0, radius, 0, PI/2);
      int opacity = int(cos(a) * 100);
      img.set(i, j, color(128, 64, 16, opacity));
    }
  }
  // Save image
  img.save("data/fire_particle.png");
}
