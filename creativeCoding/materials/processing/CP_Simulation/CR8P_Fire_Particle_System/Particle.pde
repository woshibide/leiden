class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  int time2live;

  Particle(PVector _position) {
    position = _position.copy();
    velocity = new PVector(random(-0.5, 0.5), random(-0.5, 0.5));
    acceleration = new PVector(0, -0.1);
    time2live = int(random(50));
  }

  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    time2live -= 1;
  }

  boolean dead() { return time2live <= 0; }
}
