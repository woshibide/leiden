RainbowParticleSystem rainbow;
PImage particle_img;

void setup() {
  size(400, 400);
  rainbow = new RainbowParticleSystem(width/2, height/2, 400);
  particle_img = loadImage("particle.png");
  if (particle_img == null) {
    generateImages();
    particle_img = loadImage("particle.png");
  }
}

void draw() {
  rainbow.update();
  background(100);
  imageMode(CENTER);
  for (int i = 0; i < rainbow.particles.length; i++) {
    RainbowParticle particle = rainbow.particles[i];
    PVector p = particle.position;
    float t = particle.time2live;
    color col = particle.tint;
    float s = constrain(map(t, 20, 0, 0.5, 2), 0.5, 2);
    float opa = constrain(map(t, 20, 0, 255, 0), 0, 255);
    tint(col, opa);
    image(particle_img, p.x, p.y, s*particle_img.width, s*particle_img.height);
  }  
}

void generateImages() {
  generateParticleImage();
}

void generateParticleImage() {
  int radius = 20;
  PImage img = createImage(2*radius, 2*radius, ARGB);
  for (int i = 0; i < img.width; i++) {
    for (int j = 0; j < img.height; j++) {
      float d = dist(radius, radius, i, j);
      float a = map(d, 0, radius, 0, PI/2);
      int opacity = int(cos(a) * 100);
      img.set(i, j, color(255, opacity));
    }
  }
  // Save image
  img.save("data/particle.png");
}
