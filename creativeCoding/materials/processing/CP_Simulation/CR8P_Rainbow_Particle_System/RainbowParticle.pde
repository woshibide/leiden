class RainbowParticle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  int time2live;
  color tint;
  
  RainbowParticle(PVector _position, PVector _velocity, PVector _acceleration, color _tint) {
    position = _position.copy();
    velocity = _velocity.copy();
    acceleration = _acceleration.copy();
    time2live = int(random(100));
    tint = _tint;
  }

  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    time2live -= 1;
  }

  boolean dead() { return time2live <= 0; }
}
