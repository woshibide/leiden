class RainbowParticleSystem {
  PVector position;
  RainbowParticle[] particles;
  
  RainbowParticleSystem(float x, float y, int _num_particles) {
    position = new PVector(x, y);
    particles = new RainbowParticle[_num_particles];
    push();
    colorMode(HSB, 360, 100, 100, 100);
    for (int i = 0; i < particles.length; i++) {
      particles[i] = createParticle();
    }
    pop();
  }

  RainbowParticle createParticle() {
    float ang = random(320);
    color col = color(ang, 100, 100);
    float vx = 4 * cos(radians(130 - ang/4));
    float vy = 4 * sin(radians(130 - ang/4));
    PVector vel = new PVector(vx, -vy);
    PVector acc = new PVector(0, 0.1);
    return new RainbowParticle(position, vel, acc, col);
  }

  void update() {
    push();
    colorMode(HSB, 360, 100, 100, 100);
    for (int i = 0; i < particles.length; i++) {
      particles[i].update();
      if (particles[i].dead()) {
        particles[i] = createParticle();
      }
    }
    pop();
  }
}
