ParticleSystem ps;
float radius = 10;

void setup() {
  size(400, 400);
  ps = new ParticleSystem(width/2, height/2, 100);
}

void draw() {
  ps.update();
  background(255);
  noFill();
  stroke(0, 200);
  for (int i = 0; i < ps.particles.length; i++) {
    PVector p = ps.particles[i].position;
    PVector v = ps.particles[i].velocity;
    circle(p.x, p.y, 2*radius);
    line(p.x, p.y, p.x + v.x*10, p.y + v.y*10);
  }
}
