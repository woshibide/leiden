class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;

  Particle(PVector _position) {
    position = _position.copy();
    velocity = new PVector(random(-2.5, 2.5), random(-5, 0));
    acceleration = new PVector(0, 0.1);
  }

  void update() {
    velocity.add(acceleration);
    position.add(velocity);
  }
}
