class Particle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  int time2live;

  Particle(PVector _position) {
    position = _position.copy();
    velocity = new PVector(random(-2.5, 2.5), random(-5, 0));
    acceleration = new PVector(0, 0.1);
    time2live = int(random(100, 200));
  }

  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    time2live -= 1;
  }

  boolean dead() { return time2live <= 0; }
}
