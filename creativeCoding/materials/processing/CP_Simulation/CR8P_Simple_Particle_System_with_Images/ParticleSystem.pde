class ParticleSystem {
  PVector position;
  Particle[] particles;

  ParticleSystem(float x, float y, int _num_particles) {
    position = new PVector(x, y);
    particles = new Particle[_num_particles];
    for (int i = 0; i < particles.length; i++) {
      particles[i] = new Particle(position);
    }
  }

  void update() {
    for (int i = 0; i < particles.length; i++) {
      particles[i].update();
      if (particles[i].dead()) {
        particles[i] = new Particle(position);
      }
    }
  }
}
