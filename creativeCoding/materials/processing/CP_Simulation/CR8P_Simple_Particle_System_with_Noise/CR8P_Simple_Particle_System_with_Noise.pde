ParticleSystem ps;
PImage img;

float noff = 100;
float nmag = 0.02;

void setup() {
  size(400, 400);
  ps = new ParticleSystem(width/2, height/2, 400);
  img = loadImage("particle.png");
  // Check if the image has loaded, if not the
  // file(s) is probably missing. So call the
  // generateImages() function to create the file(s). 
  if (img == null) {
    generateImages();
    img = loadImage("particle.png");
  }
  imageMode(CENTER);
}

void draw() {
  noff += 0.01;
  for (int i = 0; i < ps.particles.length; i++) {
    PVector p = ps.particles[i].position;
    float nx = 0.25 - 0.5 * noise(14 + noff + p.x * nmag);
    float ny = 0.25 - 0.5 * noise(17 + noff + p.y * nmag);
    ps.particles[i].addForce(new PVector(nx, ny));
  }
  PVector force =
    new PVector(0.001*(mouseX - width/2),
                0.001*(mouseY - height/2));
  ps.addForce(force);
  ps.update();
  background(100);
  for (int i = 0; i < ps.particles.length; i++) {
    PVector p = ps.particles[i].position;
    int t = ps.particles[i].time2live;
    tint(255, t);
    image(img, p.x, p.y);
  }
  line(width/2, height/2, mouseX, mouseY);
}

void generateImages() {
  generateParticleImage();
}

void generateParticleImage() {
  // Generate White Particle Image
  int radius = 20;
  PImage img = createImage(2*radius, 2*radius, ARGB);
  for (int i = 0; i < img.width; i++) {
    for (int j = 0; j < img.height; j++) {
      float d = dist(radius, radius, i, j);
      float a = map(d, 0, radius, 0, PI/2);
      int opacity = int(cos(a) * 200);
      img.set(i, j, color(255, opacity));
    }
  }
  // Save image
  img.save("data/particle.png");
}
