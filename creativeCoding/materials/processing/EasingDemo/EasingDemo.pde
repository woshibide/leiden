long startMillis;
long d = 2000;
float dx = 120;

void setup() {
  size(160, 460);
  pixelDensity(2);
  PFont labelFont = createFont("VestulaPro-Regular", 24);
  textFont(labelFont);
  textAlign(LEFT, CENTER);
  startMillis = millis();
}

void draw() {
  long t = millis() - startMillis;
  background(255);
  
  pushStyle();
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
  
  translate(20, 0);
  
  // Linear
  translate(0, 20);
  fill(0, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Linear", 0, 10);
  fill(0, 80, 80);
  ellipse(    dx * Linear.easeIn(t, d),  0, 8, 8);
  ellipse(   dx * Linear.easeOut(t, d), 10, 8, 8);
  ellipse( dx * Linear.easeInOut(t, d), 20, 8, 8);
  // Quad
  translate(0, 40);
  fill(20, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Quad", 0, 10);
  fill(20, 80, 80);
  ellipse(      dx * Quad.easeIn(t, d),  0, 8, 8);
  ellipse(     dx * Quad.easeOut(t, d), 10, 8, 8);
  ellipse(   dx * Quad.easeInOut(t, d), 20, 8, 8);
  // Cubic
  translate(0, 40);
  fill(40, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Cubic", 0, 10);
  fill(40, 80, 80);
  ellipse(     dx * Cubic.easeIn(t, d),  0, 8, 8);
  ellipse(    dx * Cubic.easeOut(t, d), 10, 8, 8);
  ellipse(  dx * Cubic.easeInOut(t, d), 20, 8, 8);
  // Quart
  translate(0, 40);
  fill(60, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Quart", 0, 10);
  fill(60, 80, 80);
  ellipse(     dx * Quart.easeIn(t, d),  0, 8, 8);
  ellipse(    dx * Quart.easeOut(t, d), 10, 8, 8);
  ellipse(  dx * Quart.easeInOut(t, d), 20, 8, 8);
  // Quint
  translate(0, 40);
  fill(80, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Quint", 0, 10);
  fill(80, 80, 80);
  ellipse(     dx * Quint.easeIn(t, d),  0, 8, 8);
  ellipse(    dx * Quint.easeOut(t, d), 10, 8, 8);
  ellipse(  dx * Quint.easeInOut(t, d), 20, 8, 8);
  // Sine
  translate(0, 40);
  fill(100, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Sine", 0, 10);
  fill(100, 80, 80);
  ellipse(      dx * Sine.easeIn(t, d),  0, 8, 8);
  ellipse(     dx * Sine.easeOut(t, d), 10, 8, 8);
  ellipse(   dx * Sine.easeInOut(t, d), 20, 8, 8);
  // Circ
  translate(0, 40);
  fill(120, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Circ", 0, 10);
  fill(120, 80, 80);
  ellipse(      dx * Circ.easeIn(t, d),  0, 8, 8);
  ellipse(     dx * Circ.easeOut(t, d), 10, 8, 8);
  ellipse(   dx * Circ.easeInOut(t, d), 20, 8, 8);
  // Expo
  translate(0, 40);
  fill(140, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Expo", 0, 10);
  fill(140, 80, 80);
  ellipse(      dx * Expo.easeIn(t, d),  0, 8, 8);
  ellipse(     dx * Expo.easeOut(t, d), 10, 8, 8);
  ellipse(   dx * Expo.easeInOut(t, d), 20, 8, 8);
  // Back
  translate(0, 40);
  fill(160, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Back", 0, 10);
  fill(160, 80, 80);
  ellipse(      dx * Back.easeIn(t, d),  0, 8, 8);
  ellipse(     dx * Back.easeOut(t, d), 10, 8, 8);
  ellipse(   dx * Back.easeInOut(t, d), 20, 8, 8);
  // Bounce
  translate(0, 40);
  fill(180, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Bounce", 0, 10);
  fill(180, 80, 80);
  ellipse(    dx * Bounce.easeIn(t, d),  0, 8, 8);
  ellipse(   dx * Bounce.easeOut(t, d), 10, 8, 8);
  ellipse( dx * Bounce.easeInOut(t, d), 20, 8, 8);
  // Elastic
  translate(0, 40);
  fill(200, 80, 80, 25);
  rect(-8, -8, dx + 16, 20 + 16, 8);
  fill(0, 0, 100);
  text("Elastic", 0, 10);
  fill(200, 80, 80);
  ellipse(   dx * Elastic.easeIn(t, d),  0, 8, 8);
  ellipse(  dx * Elastic.easeOut(t, d), 10, 8, 8);
  ellipse(dx * Elastic.easeInOut(t, d), 20, 8, 8);
  
  popStyle();
  
  saveFrame("frames/####.png");
  if (t > 2 * d) {
    exit();
    //startMillis = millis();
  }
}
