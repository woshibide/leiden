long t, startMillis;

long duration1 = 2000, duration2 = 4000;

int state = 0;
PFont heavyFont;
PFont lightFont;

void setup() {
  size(400, 400);
  heavyFont = createFont("ChunkFive-Regular.otf", 64);
  lightFont = createFont("Lato-Thin.ttf", 64);
}

void setState(int s) {
  startMillis = millis();
  state = s;
}

void update() {
  if (state == 0) {
    setState(1);
  } else if (state == 1) {
    if (t > duration1) {
      setState(2);
    }
  } else if (state == 2) {
    if (t > duration2) {
      setState(1);
    }
  }
  t = millis() - startMillis;
}

void draw() {
  update();
  background(255);
  if (state == 1) {
    fill(0);
    textFont(heavyFont);
    textAlign(CENTER, BASELINE);
    float h = height * Bounce.easeOut(t, duration1);
    text("HEAVY", width/2, h);
  } if (state == 2) {
    fill(0, 128, 192);
    textFont(lightFont);
    textAlign(CENTER, TOP);
    float h = height * (1 - Bounce.easeOut(t, duration2));
    text("light", width/2, h);
  }
}
