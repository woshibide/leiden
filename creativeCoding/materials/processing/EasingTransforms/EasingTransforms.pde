long t, startMillis;
int state = 0;

void setup() {
  size(600, 600);
  frameRate(60);
}

void setState(int s) {
  state = s;
  startMillis = millis();
}

void update() {
  if (state == 0) {
    setState(1);
  } else if (state == 1) {
    if (t > 2000) { setState(2); }
  } else if (state == 2) {
    if (t > 2000) { setState(3); }
  } else if (state == 3) {
    if (t > 2000) { setState(1); }
  }
  t = millis() - startMillis;
}

void draw() {
  update();
  background(255);
  rectMode(CENTER);
  noStroke();
  float x = 0;
  float y = 0;
  float r = 0;
  if (state == 1) {
    x = width/2;
    y = (100 + height/2) * Bounce.easeOut(t, 2000) - 100;
    r = 0;
  } else if (state == 2) {
    x = width/2;
    y = height/2;
    r = 90 * Elastic.easeOut(t, 2000);
  } else if (state == 3) {
    x = width/2 + (width/2 + 100) * Back.easeIn(t, 500);
    y = height/2;
    r = 0;
  }

  translate(x, y);
  pushMatrix();
  rotate(radians(r));
  fill(128, 160, 192);
  rect(0, 0, 200, 200);
  popMatrix();
  
  pushMatrix();
  rotate(radians(-2*r));
  fill(192, 128, 128);
  rect(0, 0, 100, 100);
  popMatrix();
}
