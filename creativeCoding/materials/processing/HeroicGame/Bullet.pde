class Bullet {
  float x, y, dx, dy;
  
  Bullet(float _x, float _y, float _dx, float _dy) {
    x = _x; y = _y; dx = _dx; dy = _dy;
  }
  
  void update() {
    x += dx;
    y += dy;
  }
  
  void draw() {
    push();
      noFill();
      stroke(200, 0, 0);
      strokeWeight(4);
      line(x, y, x + 4*dx, y + 4*dy);
    pop();
  }
}
