class Enemy {
  float x, y;
  float radius = 20;
  float noiseOffset;
  
  boolean alive = true;
  
  Enemy() {
    noiseOffset = random(1000);
    x = noise(noiseOffset + millis()/1000.0) * width;
    y = -radius;
  }
  
  void update() {
    for (Bullet b : bullets) {
      float d = dist(x, y, b.x, b.y);
      if (d <= radius) {
        alive = false;
      }
    }
    if (alive) {
      x = noise(noiseOffset + millis()/1000.0) * width;
      y += 1;
    }
  }
  
  void draw() {
    push();
      translate(x, y);
      noStroke();
      if (alive) {
        fill(180, 0, 200);
      } else {
        fill(180, 0, 200, 64);
      }
      circle(0, 0, 2*radius);
    pop();
  }
}
