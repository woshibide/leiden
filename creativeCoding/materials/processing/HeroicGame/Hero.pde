class Hero {
  float x, y;
  float radius = 20;
  
  boolean alive = true;
  
  PImage img;
  
  Hero() {
    x = random(radius, width - radius);
    y = random(radius, height - radius);
    img = loadImage("hero.png");
  }
  
  void update() {
    for (Enemy e : enemies) {
      float d = dist(x, y, e.x, e.y);
      if (d < radius + e.radius) {
        alive = false;
      }
    }
  }
  
  void draw() {
    push();
    translate(x, y);
    // If you want to draw a circle, use this code instead
    //noStroke();
    //if (alive) {
    //  fill(240, 220, 0);
    //} else {
    //  fill(240, 220, 0, 64);
    //}
    //circle(0, 0, 2*radius);
    imageMode(CENTER);
    if (!alive) {
      tint(255, 0, 0, 128);
    }
    image(img, 0, 0, 2*radius, 2*radius);
    pop();
  }
  
  void move(float dx, float dy) {
    if (alive) {
      x += dx;
      y += dy;
    }
  }
  
  void grow(float dr) {
    if (alive) {
      radius += dr;
    }
  }
  
  void fire() {
    if (alive) {
      Bullet b = new Bullet(x, y, 0, -2);
      bullets.add(b);
    }
  }
}
