import java.util.*;

Hero hero;

ArrayList<Bullet> bullets;
ArrayList<Enemy> enemies;

void setup() {
  size(400, 400);
  hero = new Hero();
  bullets = new ArrayList<Bullet>();
  enemies = new ArrayList<Enemy>();
}

void update() {
  if (random(1) < 0.01) {
    Enemy e = new Enemy();
    enemies.add(e);
  }
  
  for (Bullet b : bullets) {
    b.update();
  }
  for (Enemy e : enemies) {
    e.update();
  }
  hero.update();
}

void draw() {
  update();
  background(0);
  for (Bullet b : bullets) {
    b.draw();
  }
  for (Enemy e : enemies) {
    e.draw();
  }
  hero.draw();
  if (!hero.alive) {
    fill(0, 64);
    noStroke();
    rect(0, 0, width, height);
    textSize(50);
    textAlign(CENTER, CENTER);
    fill(0, 200, 0);
    text("GAME OVER", width/2, height/2);
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      hero.move(0, -20);
    } else if (keyCode == DOWN) {
      hero.move(0, 20);
    } else if (keyCode == LEFT) {
      hero.move(-20, 0);
    } else if (keyCode == RIGHT) {
      hero.move(20, 0);
    }
  }
}

void mousePressed() {
  hero.grow(10);
  hero.fire();
}

void mouseReleased() {
  hero.grow(-10);
}
