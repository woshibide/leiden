// Change the following value to explore how the map() function works.

// The values will be used to calculate the output of the following:
//
//   output = map(input, inputLow, inputHigh, outputLow, outputHigh)
//
// The display visually hows how the input is mapped from the input range,
// [inputLow, inputHigh], to the output range [outputLow, outputHigh].
//
// It will also show how map() relates to norm() and lerp() by displaying
// an intermediary value (nval), which is in the range [0, 1] 
//
//   nval = norm(input, inputLow, inputHigh)
//
//   output = lerp(outputLow, outputHigh, nval)

// If you want to play with ranges outside the default "master range"
// [-100, 100], change the following values. Just be sure to make them
// big enough to cover both the input range and the output range.

float masterLow = -100;
float masterHigh = 100;

// You're welcome to look at the rest of the code, but this was written
// as a quick tool for exploring how map() works and not an example of
// how to write great code.

// TODO: Make the display interactive / convert to P5.js and embed in BS

float left, top, right, bottom;

float input, inputLow, inputHigh;
float norm;
float output, outputLow, outputHigh;

float inputY, inputX, inputLowX, inputHighX;
float normY, normX, normLowX, normHighX;
float outputY, outputX, outputLowX, outputHighX;

void setup() {
  size(600, 300);
  pixelDensity(2);

  textFont(createFont("Inconsolata-Regular", 14));

  left = 40;
  top = 70;
  right = width - 40;
  bottom = height - 40;
  
  // Initial values
  input = 8;
  inputLow = -10;
  inputHigh = 30;
  outputLow = 20;
  outputHigh = 60;

  inputY = top;
  normY = (top + bottom)/2;
  outputY = bottom;

  normLowX = left + 0.125 * (right-left);
  normHighX = right - 0.125 * (right-left);
}

void update() {
  norm = norm(input, inputLow, inputHigh);
  output = map(input, inputLow, inputHigh, outputLow, outputHigh);

  inputLowX   = map(inputLow,   masterLow, masterHigh, left, right);
  inputHighX  = map(inputHigh,  masterLow, masterHigh, left, right);
  inputX      = map(input,      masterLow, masterHigh, left, right);
  normX       = map(norm, 0, 1, normLowX, normHighX);
  outputLowX  = map(outputLow,  masterLow, masterHigh, left, right);
  outputHighX = map(outputHigh, masterLow, masterHigh, left, right);
  outputX     = map(output,     masterLow, masterHigh, left, right);
}

void draw() {
  background(255);
  update();
  drawExpression();
  drawMapping(input, inputLow, inputHigh, outputLow, outputHigh);
}

void drawExpression() {
  push();
  fill(0);
  textAlign(CENTER, BASELINE);
  text(
    "map(" +
    nf(input) + ", " +
    nf(inputLow) + ", " +
    nf(inputHigh) + ", " +
    nf(outputLow) + ", " +
    nf(outputHigh) + ") = " +
    nf(output),
    width/2, top/2);
  pop();
}

void drawMapping(float input, float inputLow, float inputHigh, float outputLow, float outputHigh) {
  push();
  noStroke();
  fill(240, 180, 0, 64);
  quad(inputLowX, inputY, inputHighX, inputY, normHighX, normY, normLowX, normY);
  fill(0, 120, 240, 64);
  quad(normLowX, normY, normHighX, normY, outputHighX, outputY, outputLowX, outputY);

  fill(0, 128);
  textAlign(CENTER, BASELINE);
  text(
    "norm(" +
    nf(input) + ", " +
    nf(inputLow) + ", " +
    nf(inputHigh) + ") = " +
    nf(norm), (left + right)/2, (inputY + normY)/2);

  text(
    "lerp(" +
    nf(outputLow) + ", " +
    nf(outputHigh) + ", " +
    nf(norm) + ") = " +
    nf(lerp(outputLow, outputHigh, norm),0,0), (left + right)/2, (normY + outputY)/2);
    
  drawScale(left,     inputY,  right,     20, masterLow, masterHigh);
  drawScale(normLowX, normY,   normHighX, 10, 0, 1);
  drawScale(left,     outputY, right,     20, masterLow, masterHigh);
  
  stroke(128, 0, 0, 192);
  line(inputX, inputY, normX, normY);
  line(normX, normY, outputX, outputY);

  noStroke();
  // Draw input range handles
  fill(240, 180, 0);
  ellipse(inputLowX,  inputY, 8, 8);
  ellipse(inputHighX, inputY, 8, 8);
  // Draw output range handles
  fill(0, 120, 240);
  ellipse(outputLowX,  outputY, 8, 8);
  ellipse(outputHighX, outputY, 8, 8);
  // Draw input handle  
  fill(128, 0, 0, 192);
  circle(inputX, inputY, 8);
  textAlign(CENTER, TOP);
  textSize(constrain((right-left)/40, 8, 10));
  text(nf(input,  0, 0), inputX,  inputY + 8);
  text(nf(norm,   0, 0), normX,   normY + 8);
  text(nf(output, 0, 0), outputX, outputY + 8);
  pop();
}

void drawScale(float left, float top, float right, float subs, float low, float high) {
  push();
  stroke(192);
  line(left, top, right, top);
  float dx = (right-left) / subs;
  for (int i = 0; i <= subs; i++) {
    float sx = left + i * dx;
    line(sx, top, sx, top - 4);
    float lx = low + i * ((high - low)/subs);
    fill(0, 128);
    textSize(constrain((right-left)/40, 8, 10));
    text(nf(lx,0,0), sx, top - 6);
  }
  pop();
}

void mouseDragged() {
  if (dist(pmouseX, pmouseY, inputX, inputY) < 16) {
    input = int(constrain(map(mouseX, inputLowX, inputHighX, inputLow, inputHigh), inputLow, inputHigh));
  } else if (dist(pmouseX, pmouseY, inputLowX, inputY) < 16) {
    inputLow = int(constrain(map(mouseX, left, right, masterLow, masterHigh), masterLow, masterHigh));
  } else if (dist(pmouseX, pmouseY, inputHighX, inputY) < 16) {
    inputHigh = int(constrain(map(mouseX, left, right, masterLow, masterHigh), masterLow, masterHigh));
  } else if (dist(pmouseX, pmouseY, outputLowX, outputY) < 16) {
    outputLow = int(constrain(map(mouseX, left, right, masterLow, masterHigh), masterLow, masterHigh));
  } else if (dist(pmouseX, pmouseY, outputHighX, outputY) < 16) {
    outputHigh = int(constrain(map(mouseX, left, right, masterLow, masterHigh), masterLow, masterHigh));
  }
}
