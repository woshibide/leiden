import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;

PImage[] loadFrames(String fileMask) {
  // Get the data folder
  File folder = dataFile("");
  
  // Parse the fileMask into prefix and suffix
  String[] tokens = fileMask.split("#+\\.");
  String prefix = tokens[0];
  String suffix = tokens[1];
  
  // Load a filtered array of files
  File[] files = folder.listFiles(
    new FileFilter() {
      public boolean accept(File f) {
        String filename = f.getName();
        return filename.startsWith(prefix) &&
               filename.endsWith(suffix); 
      }
  });
  
  // Sort the files by name
  Arrays.sort(files, new Comparator<File>() {
    public int compare(File f1, File f2) {
      return f1.getName().compareTo(f2.getName());
    }
  });
  
  // Load images
  PImage[] images = new PImage[files.length];
  for (int i = 0; i < files.length; i++) {
    images[i] = loadImage(files[i].getPath());
  }
  
  return images;
}
