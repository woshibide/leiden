PImage[] loadFrames;
PImage[] animFrames;

void setup() {
  size(400, 300);
  frameRate(24);
  loadFrames = loadFrames("load_####.gif");
  animFrames = loadFrames("cube_####.gif");
  imageMode(CENTER);
}

void draw() {
  if (millis() < 5000) {
    background(117, 68, 226);
    image(loadFrames[frameCount % loadFrames.length], width/2, height/2);
  } else {
    background(27);
    image(animFrames[frameCount % animFrames.length], width/2, height/2);
  }
}
